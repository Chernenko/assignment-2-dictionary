%include "lib.inc"   

section .text
global find_word

find_word:
    push rsi      
    mov rcx, rsi  
.loop:
    add rcx, 8      
    mov rsi, rcx      
    push rcx
    call string_equals  
    pop rcx
    test rax, rax 
    jnz .success    
    sub rcx, 8         
    mov rcx, [rcx]    
    test rcx, rcx 
    jz .fail      
    jmp .loop
.success:
    mov rax, rsi    
    jmp .rev
.fail:
    xor rax, rax
.rev:
    pop rsi
    ret           

    


%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define BUFFER_SIZE 256

section .data
long_str: db "ERROR: Input string is too long", 0
no_value: db "ERROR: Value is not found", 0

section .bss
    buffer: resb BUFFER_SIZE


section .text
global _start

_start:
    mov rdi, buffer
    mov rsi, BUFFER_SIZE
    call read_string
    test rax, rax
    jz .error_buffer
    mov rdi, buffer
    mov rsi, DICT_START
    call find_word
    test rax, rax
    jz .not_found
    mov rdi, rax
    call print_string
    mov rdi, 0
    jmp exit

.error_buffer:
    mov rdi, long_str
    jmp exit

.not_found:
    mov rdi, no_value
    jmp exit



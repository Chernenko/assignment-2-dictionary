%define WRITE_SYSCALL_NUMBER 1
%define SYSCALL_EXIT 60

%define STD_OUT 1

%define SYMB_NL `\n`
%define SYMB_SPACE ` `
%define SYMB_TAB `\t`
%define SYMB_NUM_MIN '0'
%define SYMB_NUM_MAX '9'
section .text
 
 global exit
 global string_length
 global print_string
 global print_newline
 global print_char
 global print_uint
 global print_int
 global string_equals
 global read_char
 global read_word
 global parse_uint
 global parse_int
 global string_copy

 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax,
    ret 
    mov rax, SYSCALL_EXIT
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    ret
    push rdi
    push rcx
	xor rax, rax
	xor rcx, rcx
.loop:
	mov cl, byte [rdi + rax]
	test cl, cl
	jz .end
	inc rax
	jmp .loop
.end:
	pop rcx
	pop rdi
	ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    ret
	push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rax, WRITE_SYSCALL_NUMBER
	mov rdi, STD_OUT
	syscall
	ret


; Переводит строку( выводит символ с кодом 0xA)
print_newline:
    mov rdi, SYMB_NL


; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    ret
    push rdi    
    mov rax, 1   
	mov rdi, 1   
	mov rsi, rsp   
	mov rdx, 1      
	syscall          
	pop rdi       
	ret            

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    ret
        mov     r9, 10
	mov     rax, rdi
	mov     rcx, rsp
	sub     rsp, 32
	dec     rcx
	mov     byte[rcx], 0
.loop:
	xor     rdx, rdx
	div     r9
	add     rdx, '0'
	dec     rcx
	mov     byte[rcx], dl
	test    rax, rax
	jne     .loop
.print:
	mov     rdi, rcx
	call    print_string
	add     rsp, 32
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    ret
        mov r11, -1
	mov rax, rdi
	imul r11
        test rax, rax	

	jle	.pos

	.neg:		
	mov	rdi, '-'
	push rax		
	call print_char	
	pop	rdi			
	call print_uint
	ret
.pos:
	jmp print_uint
	ret




; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    ret
    xor rcx, rcx
.string_equals_loop:
	mov dl, byte[rdi + rcx]
	mov al, byte[rsi + rcx]
	cmp dl, al
	jne .string_equals_not
	test al, al
	je .string_equals_yes
	inc rcx
	jmp .string_equals_loop
.string_equals_not:
	xor rax, rax
	ret
.string_equals_yes:
	mov rax, 1
	ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    ret 
    mov rdi, 0
	mov rsi, rsp
	mov rdx, STD_OUT
	syscall
	test rax, rax
	je .end
	pop rax
	ret 
	.end:
	   pop rax
	   mov rax, 0
	   ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    ret
    push r12 
	push r13
	push r14
	mov r12, rdi 
	mov r13, rsi 
	dec r13 
	xor r14, r14 
.skip: 
	call read_char
	cmp rax, SYMB_SPACE
	je .skip
	cmp rax, SYMB_TAB
	je .skip
	cmp rax, SYMB_NL
	je .skip
.loop: 
	mov [r12+r14], rax
	test rax, rax
	jz .ret 
	cmp rax, SYMB_SPACE
	je .ret 
	cmp rax, SYMB_TAB
	je .ret 
	cmp rax, SYMB_NL
	je .ret 
	cmp r13, r14
	je .ret_err ; +`\0` 
	inc r14
	call read_char
	jmp .loop
.ret_err:
	xor r12, r12
	xor r14, r14
.ret:
	mov rax, r12 
	mov rdx, r14 
	pop r14
	pop r13
	pop r12
	ret


 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    ret
    xor rsi, rsi			
	xor rax, rax		
	mov r10, 10			
.loop:
	xor rcx, rcx	
	mov cl, [rdi + rsi]	
	cmp cl, SYMB_NUM_MIN		
	jb .ret			
	cmp cl, SYMB_NUM_MAX		
	ja .ret 			
	sub rcx, SYMB_NUM_MIN		
	push rdx	
	mul r10		
	pop rdx			
	add rax, rcx	
	inc rsi			
	jmp .loop 		
.ret:
	mov rdx, rsi
	ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    ret 
   mov	r11, -1			
	xor	rsi, rsi		
	cmp	byte[rdi], '-'	
	jz	.neg		
	cmp	byte[rdi], '+'	
	jz .plus		
.pos:
	call parse_uint
	cmp	sil, '+'	
	jz	.add_plus
	ret				
																
.add_plus:
	inc	rdx		
	ret

.plus:
	mov	rsi, rdi	
	inc	rdi			
	jmp	.pos		

.neg:
	inc	rdi		
	call parse_uint	
	inc	rdx			
	push rdx		
	imul r11		
	pop	rdx			
	ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    ret
	xor rcx, rcx
.string_copy_loop:
	cmp byte[rdi + rcx], 0
	jz .string_copy_end
	mov al, byte[rdi + rcx]
	mov byte[rsi + rcx], al
	inc rcx
	cmp rdx, rcx
	jl .string_copy_wrong
	xor rax, rax
	jmp .string_copy_loop
.string_copy_end:
	cmp rcx, rdx
    jz .string_copy_last
	mov byte[rsi + rcx], 0
.string_copy_last:
	mov rax, rcx
	ret
.string_copy_wrong:
	xor rax, rax
	ret
